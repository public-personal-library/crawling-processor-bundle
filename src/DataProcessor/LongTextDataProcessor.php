<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\CrawlingProcessorBundle\DataProcessor;

use Kematjaya\CrawlingProcessorBundle\DataProcessor\AbstractDataProcessor;
use Facebook\WebDriver\WebDriverElement;

/**
 * Description of LongTextDataProcessor
 *
 * @author programmer
 */
class LongTextDataProcessor extends AbstractDataProcessor 
{
    /**
     * 
     * @param WebDriverElement $element
     * @return string output
     */
    public function process(WebDriverElement $element) 
    {
        $originText = trim($element->getText());
        $text = $this->getMaxLength() ? substr($originText, 0, $this->getMaxLength()) : $originText;
          
        return (strlen($originText) !== strlen($text)) ? sprintf("%s...", $text) : $text;
    }

    protected function getMaxLength():?int
    {
        return null;
    }
}
