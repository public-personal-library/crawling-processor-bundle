<?php

/**
 * This file is part of the web-scraping.
 */

namespace Kematjaya\CrawlingProcessorBundle\DataProcessor;


use Kematjaya\PriceBundle\Lib\AbstractDateFormat;
use Facebook\WebDriver\WebDriverElement;

/**
 * @package App\DataProcessor
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
class IndonesianDateDataProcessor extends AbstractDataProcessor
{
    /**
     * 
     * @var AbstractDateFormat
     */
    private $dateFormat;
    
    public function __construct(AbstractDateFormat $dateFormat) 
    {
        $this->dateFormat = $dateFormat;
    }
    
    /**
     * 
     * @param WebDriverElement $element
     * @return \DateTimeInterface
     */
    public function process(WebDriverElement $element) 
    {
        $text = trim($element->getText());
        if (false !== strpos($text, ',')) {
            
            return $this->dateFormat->reverse($text);
        }
        
        $texts = array_filter(explode(" ", $text));
        if (isset($texts[3])) {
            $text = sprintf("%s %s %s, %s", $texts[0], $texts[1], $texts[2], $texts[3]);
        }
        
        return $this->dateFormat->reverse($text);
    }

}
