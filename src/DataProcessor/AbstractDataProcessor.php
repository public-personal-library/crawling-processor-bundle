<?php

/**
 * This file is part of the web-scraping.
 */

namespace Kematjaya\CrawlingProcessorBundle\DataProcessor;

use Facebook\WebDriver\WebDriverElement;

/**
 * @package Kematjaya\CrawlingProcessorBundle\DataProcessor
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
abstract class AbstractDataProcessor 
{
    
    const TAG_NAME = 'kematjaya.web_driver_element_processor';
    
    /**
     * @param WebDriverElement $element
     */
    abstract function process(WebDriverElement $element);
}
