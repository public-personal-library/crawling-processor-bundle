<?php

/**
 * This file is part of the web-scraping.
 */

namespace Kematjaya\CrawlingProcessorBundle\DataProcessor;

use Facebook\WebDriver\WebDriverElement;

/**
 * @package App\DataProcessor
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
abstract class PriceDataProcessor extends AbstractDataProcessor
{
    
    /**
     * 
     * @param WebDriverElement $element
     * @return float
     */
    public function process(WebDriverElement $element) 
    {
        $result = $this->clear($element->getText());
        if (!is_numeric($result)) {
            
            return 0;
        }
        
        return (float) $result;
    }
    
    protected function clear(string $value):string
    {
        $replacer = [
            ',' => '.',
            '.' => '',
            ' ' => ''
        ];
        
        $result = '';
        $text = str_replace($this->pricePrefix(), '', trim($value));
        for ($i = 0; $i < strlen($text); $i++) {
            if (is_numeric($text[$i])) {
                $result .= $text[$i];
                continue;
            }
            
            if (!isset($replacer[$text[$i]])) {
                $result .= $text[$i];
                continue;
            }
            
            $result .= $replacer[$text[$i]];
        }
        
        return $result;
    }
    
    protected function pricePrefix():?string
    {
        return '';
    }

}
