<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\CrawlingProcessorBundle\DataProcessor;

use Facebook\WebDriver\WebDriverElement;

/**
 * Description of IndonesianPriceDataProcessor
 *
 * @author guest
 */
class IndonesianPriceDataProcessor extends PriceDataProcessor
{
    /**
     * 
     * @param WebDriverElement $element
     * @return type
     */
    public function process(WebDriverElement $element) 
    {
        return $this->replace(
            $this->clear(
                $element->getText()
            )
        );
    }
    
    /**
     * 
     * @param string $value
     * @return float
     */
    protected function replace(string $value): float
    {
        $arr = [
            'M' => 1000000000,
            'Jt' => 1000000,
        ];
        foreach ($arr as $symbol => $val) {
            if (false === strpos($value, $symbol)) {
                continue;
            }
            
            $total = (float) str_replace($symbol, '', $value);
            
            return $total * $val;
        }
        
        return (float) $value;
    }
    
    protected function pricePrefix():?string
    {
        return 'Rp';
    }
}
