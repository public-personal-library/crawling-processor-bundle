<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\CrawlingProcessorBundle\DataProcessor;

use Facebook\WebDriver\WebDriverElement;

/**
 * Description of IntegerDataProcessor
 *
 * @author guest
 */
class IntegerDataProcessor extends TextDataProcessor
{
    public function process(WebDriverElement $element) 
    {
        $text = parent::process($element);
        
        return (int) $text;
    }
}
