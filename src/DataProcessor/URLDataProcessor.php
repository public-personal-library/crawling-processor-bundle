<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\CrawlingProcessorBundle\DataProcessor;

use Symfony\Component\Panther\DomCrawler\Crawler;
use Facebook\WebDriver\WebDriverElement;

/**
 * Description of URLDataProcessor
 *
 * @author guest
 */
class URLDataProcessor extends AbstractDataProcessor 
{
    //put your code here
    public function process(WebDriverElement $element) 
    {
        $args = func_get_args();
        $crawler = null;
        foreach ($args as $argument) {
            if ($argument instanceof Crawler) {
                $crawler = $argument;
                break;
            }
        }
        
        $href = trim($element->getAttribute('href'));
        if (!$crawler instanceof Crawler) {
            
            return $href;
        }
        
        $links = array_values(array_filter(
            explode(DIRECTORY_SEPARATOR, $href)
        ));
        if (empty($links)) {
            
            return $href;
        }

        $prevUrls = array_filter(explode($links[0], $crawler->getUri()));
        if (!isset($prevUrls[0])) {

            return $href;
        }
        
        return str_replace("///", '/', sprintf('%s/%s', $prevUrls[0], $href));
    }

}
