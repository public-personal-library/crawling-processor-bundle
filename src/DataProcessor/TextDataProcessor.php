<?php

/**
 * This file is part of the web-scraping.
 */

namespace Kematjaya\CrawlingProcessorBundle\DataProcessor;

/**
 * @package Kematjaya\CrawlingProcessorBundle\DataProcessor
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
class TextDataProcessor extends LongTextDataProcessor
{
    protected function getMaxLength():?int
    {
        return 250;
    }
}
