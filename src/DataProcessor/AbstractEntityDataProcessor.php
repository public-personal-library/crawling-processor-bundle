<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\CrawlingProcessorBundle\DataProcessor;

use Doctrine\ORM\EntityManagerInterface;
use Facebook\WebDriver\WebDriverElement;

/**
 * Description of AbstractEntityDataProcessor
 *
 * @author guest
 */
abstract class AbstractEntityDataProcessor extends AbstractDataProcessor
{
    /**
     * 
     * @var EntityManagerInterface
     */
    protected $entityManager;
    
    public function __construct(EntityManagerInterface $entityManager) 
    {
        $this->entityManager = $entityManager;
    }
    
    public function process(WebDriverElement $element)
    {
        $value = $this->getValue($element);
        $entityName = $this->getEntityName();
        $object = $this->entityManager
                ->getRepository($entityName)
                ->findOneBy([
                    $this->getAttribute() => $value
                ]);
        
        if (!$object) {
            $scheduleObject = $this->findInUnitOfWork($value);
            if ($scheduleObject) {
                
                return $scheduleObject;
            }
            
            $object = $this->createObject($value);
            
            $this->entityManager->persist($object);
        }
        
        return $object;
    }
    
    protected function findInUnitOfWork(string $value)
    {
        $uow = $this->entityManager->getUnitOfWork();
        if (empty($uow->getScheduledEntityInsertions())) {
            
            return null;
        }
        
        $class = $this->getEntityName();
        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if (!$entity instanceof $class) {
                continue;
            }
            
            if (!$this->isValidEntity($entity, $value)) {
                continue;
            }
            
            return $entity;
        }
        
        return null;
    }
    
    protected function getValue(WebDriverElement $element):string
    {
        return trim(strtoupper($element->getText()));
    }
    
    abstract public function getAttribute():string;
    
    abstract public function getEntityName():string;
    
    protected function isValidEntity($entity, string $value) :bool
    {
        return true;
    }
    
    /**
     * @return mixed Object of entity
     */
    abstract protected function createObject(string $value);
    
}
