<?php

/**
 * This file is part of the web-scraping.
 */

namespace Kematjaya\CrawlingProcessorBundle\CompilerPass;

use Kematjaya\CrawlingProcessorBundle\Builder\DataProcessorBuilderInterface;
use Kematjaya\CrawlingProcessorBundle\DataProcessor\AbstractDataProcessor;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

/**
 * @package App\CompilerPass
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
class DataProcessorCompilerPass implements CompilerPassInterface
{
    
    public function process(ContainerBuilder $container) 
    {
        $definition = $container->findDefinition(DataProcessorBuilderInterface::class);
        $taggedServices = $container->findTaggedServiceIds(AbstractDataProcessor::TAG_NAME);
        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addProcessor', [new Reference($id)]);
        }
    }

}
