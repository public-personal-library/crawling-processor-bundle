<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\CrawlingProcessorBundle;

use Kematjaya\CrawlingProcessorBundle\CompilerPass\DataProcessorCompilerPass;
use Kematjaya\CrawlingProcessorBundle\DataProcessor\AbstractDataProcessor;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Description of CrawlingProcessorBundle
 *
 * @author guest
 */
class CrawlingProcessorBundle extends Bundle 
{
    public function build(ContainerBuilder $container) 
    {
        $container->registerForAutoconfiguration(AbstractDataProcessor::class)
                ->addTag(AbstractDataProcessor::TAG_NAME);
        
        $container->addCompilerPass(new DataProcessorCompilerPass());
        
        parent::build($container);
    }
}
