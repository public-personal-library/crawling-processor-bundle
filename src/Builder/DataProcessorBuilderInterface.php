<?php

/**
 * This file is part of the web-scraping.
 */

namespace Kematjaya\CrawlingProcessorBundle\Builder;

use Kematjaya\CrawlingProcessorBundle\DataProcessor\AbstractDataProcessor;

/**
 * @package Kematjaya\CrawlingProcessorBundle\Builder
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
interface DataProcessorBuilderInterface 
{
    public function addProcessor(AbstractDataProcessor $chart): self;

    public function getProcessor(string $className): AbstractDataProcessor;
}
