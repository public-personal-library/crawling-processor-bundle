<?php

/**
 * This file is part of the web-scraping.
 */

namespace Kematjaya\CrawlingProcessorBundle\Builder;

use Kematjaya\CrawlingProcessorBundle\DataProcessor\AbstractDataProcessor;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @package Kematjaya\CrawlingProcessorBundle\Builder
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
class DataProcessorBuilder implements DataProcessorBuilderInterface
{
    /**
     * 
     * @var Collection
     */
    private $processor;
    
    public function __construct() 
    {
        $this->processor = new ArrayCollection();
    }
    
    public function addProcessor(AbstractDataProcessor $chart): DataProcessorBuilderInterface 
    {
        if (!$this->processor->contains($chart)) {
            $this->processor->add($chart);
        }
        
        return $this;
    }

    public function getProcessor(string $className): AbstractDataProcessor 
    {
        $collection = $this->processor->filter(function (AbstractDataProcessor $chart) use ($className) {
            
            return $className === get_class($chart);
        });
        
        if ($collection->isEmpty()) {
            
            throw new \Exception("cannot find processor: ". $className);
        }
        
        return $collection->first();
    }
}
